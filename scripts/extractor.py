#!/usr/bin/python3
from mimetypes import guess_extension, guess_type

PNG_EXTRACTOR_MAGIC = 'plOD'

PNG_SIGNATURE = [137, 80, 78, 71, 13, 10, 26, 10]
PNG_SIGNATURE_SIZE = 8

PNG_SIZE_LEN = 4
PNG_TYPE_LEN = 4
PNG_CRC_LEN = 4


def swap32(x):
    #  stolen from: https://stackoverflow.com/questions/27506474/how-to-byte-swap-a-32-bit-integer-in-python
    return int.from_bytes(x.to_bytes(4, byteorder='little'), byteorder='big', signed=False)


class PngChunk():
    def __init__(self) -> None:
        pass


class PngExtractor():
    def __init__(
        self, source_filename: str = None,
        destination_filename: str = None,
        chunk_magic: str = PNG_EXTRACTOR_MAGIC
    ) -> None:
        self._source_filename = source_filename
        self._destination_filename = destination_filename
        self._magic = chunk_magic,
        self._source_file = None,
        self._destination_data = b''
        self._ext = ".file"

    def convert_and_save(self) -> bool:
        if self._destination_filename is not None and self._source_filename is not None:
            if self._source_open() is True:
                if self._source_read():
                    with open(self._destination_filename, 'wb+') as file:
                        print('\nSaving payload to "{:s}"'.format(
                            self._destination_filename))
                        file.write(self._destination_data)
                        file.flush()
                        file.close()

                    # TODO: this shit doesn't work, make it work
                    # self._ext = guess_extension(
                    #   guess_type(self._source_filename))

                    # print(guess_type(self._source_filename, False))

                    # with open('{:s}{:s}'.format(self._destination_filename.split('.')[0], self._ext), 'wb+') as ext_file:
                    # ext_file.write()
                    # ext_file.write(self._destination_data)
                    # ext_file.flush()
                    # ext_file.close()

        self._source_file.close()
        return False

    def _source_open(self) -> bool:
        if self._source_filename is not None:
            self._source_file = open(self._source_filename, 'rb')
            if self._source_file is not None:
                buff = self._source_file.read(PNG_SIGNATURE_SIZE)
                if set(buff) == set(PNG_SIGNATURE):
                    return True
                else:
                    print('[ERROR]: PNG signature mismatch\nPNG: {:x}\nSRC: {:x}\n'.format(
                        int.from_bytes(PNG_SIGNATURE, 'little'),
                        int.from_bytes(buff, 'little'),
                    ))

            return False
        else:
            print('[ERROR]: Unable to open source file: no path provided.')

        return False

    def _source_read(self) -> bool:
        if self._source_file is not None:
            self._png_chunk_read()

            return True

        return False

    def _png_chunk_read(self) -> bool:
        while True:
            # print('pos: {}\n'.format(self._source_file.tell()))
            chunk_size = int.from_bytes(
                self._source_file.read(PNG_SIZE_LEN),
                'big',
            )

            chunk_type = self._source_file.read(PNG_TYPE_LEN)
            chunk_data = self._source_file.read(chunk_size)

            chunk_crc = int.from_bytes(
                self._source_file.read(PNG_CRC_LEN),
                'big',
            )

            print('=====\nChunk\n  size: {:d}\n  type: {:s}\n  crc: 0x{:08x}'.format(
                chunk_size, str(chunk_type), chunk_crc
            ))

            if chunk_type.decode('ascii') == self._magic[0]:
                print('  data: {}'.format(chunk_data[:32]))
                self._destination_data = chunk_data
                return True
            else:
                print('[INFO]: Chunk type mismatch. Expected "{:s}" and got "{:s}"'.format(
                    self._magic[0], chunk_type.decode('ascii')
                ))

            if chunk_size <= 0:  # TODO: proper handling of IEND chunk
                break

        return False


def test():
    pngex = PngExtractor('output.png', 'extracted.file', PNG_EXTRACTOR_MAGIC)
    pngex.convert_and_save()


test()
