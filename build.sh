#!/bin/bash

# set -xe
CC=gcc

TARGET="png"
BUILD_DIR="build"
INCLUDES="include"
LIBS=

CFLAGS="-Wall -Wextra --pedantic"

rm -fr $BUILD_DIR
mkdir $BUILD_DIR
$CC $CFLAGS -o $BUILD_DIR/$TARGET -I $INCLUDES png.c $LIBS
